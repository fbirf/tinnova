# Teste do Fabiano Rodrigues Felix para o Tinnova



## Introdução

Este é um projeto que utiliza Spring Boot, banco de dados em memória H2 e documentação Swagger.

Sua url é *http://localhost:8080/tinnova*. Por ela é acessado a página inicial da API.

### Banco de Dados
Ele utiliza o banco de dados em memória **H2**. O script **data.sql** possui uns dados iniciais (fabricantes de veículos). 

##### Dados de acesso:

**Usuário**: *sa*

**Senha**: *SEM_SENHA*

**URL**: *http://localhost:8080/tinnova/h2-console*

### Documentação:
O Swagger contendo todas as requisições documentadas pode ser acessada pela url http://localhost:8080/tinnova/swagger-ui/index.html. 

