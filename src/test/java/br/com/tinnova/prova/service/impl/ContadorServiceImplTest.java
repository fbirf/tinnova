package br.com.tinnova.prova.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.tinnova.prova.dto.response.ContadorResponse;
import br.com.tinnova.prova.entity.Automovel;
import br.com.tinnova.prova.entity.Marca;
import br.com.tinnova.prova.repository.AutomovelRepository;

@SpringBootTest
public class ContadorServiceImplTest{
	
	@Mock
	private AutomovelRepository automovelRepository;
	
	@InjectMocks
	private ContadorServiceImpl contadorServiceImpl;
	
	@Test
	public void testListaPorDecadaDeFabricacao() {
		
		List<Automovel> automovelList = new ArrayList<>();
		
		Automovel a1 = new Automovel();
		a1.setAno(1935);
		automovelList.add(a1);
		
		Automovel a2 = new Automovel();
		a2.setAno(1978);
		automovelList.add(a2);
		
		Automovel a3 = new Automovel();
		a3.setAno(1995);
		automovelList.add(a3);
		
		Automovel a4 = new Automovel();
		a4.setAno(1935);
		automovelList.add(a4);
		
		Automovel a5 = new Automovel();
		a5.setAno(2001);
		automovelList.add(a5);
		
		Automovel a6 = new Automovel();
		a6.setAno(2010);
		automovelList.add(a6);
		
		Automovel a7 = new Automovel();
		a7.setAno(1984);
		automovelList.add(a7);
		
		Automovel a8 = new Automovel();
		a8.setAno(1945);
		automovelList.add(a8);
		
		Automovel a9 = new Automovel();
		a9.setAno(1921);
		automovelList.add(a9);
		
		Automovel a10 = new Automovel();
		a10.setAno(1910);
		automovelList.add(a10);
		
		Automovel a11 = new Automovel();
		a11.setAno(2002);
		automovelList.add(a11);
		
		Automovel a12 = new Automovel();
		a12.setAno(2001);
		automovelList.add(a12);
		
		Automovel a13 = new Automovel();
		a13.setAno(2021);
		automovelList.add(a13);
		
		Automovel a14 = new Automovel();
		a14.setAno(2022);
		automovelList.add(a14);
		
		Automovel a15 = new Automovel();
		a15.setAno(2013);
		automovelList.add(a15);
		
		when(automovelRepository.findAll()).thenReturn(automovelList);
		
		List<ContadorResponse> retornoList = contadorServiceImpl.listaContador("D");
		assertTrue(!retornoList.isEmpty());
		assertEquals(3, retornoList.get(1).getQuantidade());
		assertEquals(2, retornoList.get(8).getQuantidade());
		
	}
	
	@Test
	public void testListaPorFabricante() {
		
		List<Automovel> automovelList = new ArrayList<>();
		
		Marca gm = new Marca();
		gm.setCodigo("GM");
		gm.setNome("Chevrollet");
		
		Marca vw = new Marca();
		vw.setCodigo("VW");
		vw.setNome("Volks Wagen");
		
		Marca mercedes = new Marca();
		mercedes.setCodigo("MB");
		mercedes.setNome("Mercedes Benz");
		
		Marca fiat = new Marca();
		fiat.setCodigo("FIAT");
		fiat.setNome("Fiat");
		
		Automovel a1 = new Automovel();
		a1.setMarca(gm);
		automovelList.add(a1);
		
		Automovel a2 = new Automovel();
		a2.setMarca(gm);
		automovelList.add(a2);
		
		Automovel a3 = new Automovel();
		a3.setMarca(gm);
		automovelList.add(a3);
		
		Automovel a4 = new Automovel();
		a4.setMarca(vw);
		automovelList.add(a4);
		
		Automovel a5 = new Automovel();
		a5.setMarca(vw);
		automovelList.add(a5);
		
		Automovel a6 = new Automovel();
		a6.setMarca(mercedes);
		automovelList.add(a6);
		
		Automovel a7 = new Automovel();
		a7.setMarca(fiat);
		automovelList.add(a7);
		
		Automovel a8 = new Automovel();
		a8.setMarca(fiat);
		automovelList.add(a8);
		
		Automovel a9 = new Automovel();
		a9.setMarca(fiat);
		automovelList.add(a9);
		
		Automovel a10 = new Automovel();
		a10.setMarca(fiat);
		automovelList.add(a10);
		
		when(automovelRepository.findAll()).thenReturn(automovelList);
		List<ContadorResponse> retornoList = contadorServiceImpl.listaContador("F");
		assertTrue(!retornoList.isEmpty());
		
		assertEquals(2, retornoList.get(0).getQuantidade());
		assertEquals(1, retornoList.get(1).getQuantidade());
		assertEquals(3, retornoList.get(2).getQuantidade());
		assertEquals(4, retornoList.get(3).getQuantidade());
	}
	
}
