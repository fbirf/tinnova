package br.com.tinnova.prova.service.impl;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.tinnova.prova.repository.AutomovelRepository;
import br.com.tinnova.prova.repository.MarcaRepository;

@SpringBootTest
public class AutomovelServiceImplTest{
	
	@Mock
	private AutomovelRepository automovelRepository;
	
	@Mock
	private MarcaRepository marcaRepository;
	
	@InjectMocks
	private AutomovelServiceImpl automovelServiceImpl;

}
