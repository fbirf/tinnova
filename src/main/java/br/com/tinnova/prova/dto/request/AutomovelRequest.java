package br.com.tinnova.prova.dto.request;

public class AutomovelRequest {

	private int ano;
	private String descricao;
	private String marca;
	private String veiculo;
	private boolean vendido;

	public int getAno() {
		return this.ano;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public String getMarca() {
		return this.marca;
	}

	public String getVeiculo() {
		return this.veiculo;
	}

	public boolean isVendido() {
		return this.vendido;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}

	public void setVendido(boolean vendido) {
		this.vendido = vendido;
	}

}
