package br.com.tinnova.prova.dto.response;

public class ContadorResponse {
	
	private String descricaoItem;
	
	private long quantidade;

	public String getDescricaoItem() {
		return descricaoItem;
	}

	public void setDescricaoItem(String descriccaoItem) {
		this.descricaoItem = descriccaoItem;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}
	
}
