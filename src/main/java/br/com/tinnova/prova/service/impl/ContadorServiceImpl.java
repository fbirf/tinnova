package br.com.tinnova.prova.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.tinnova.prova.dto.response.ContadorResponse;
import br.com.tinnova.prova.entity.Automovel;
import br.com.tinnova.prova.entity.Marca;
import br.com.tinnova.prova.repository.AutomovelRepository;
import br.com.tinnova.prova.service.ContadorService;

@Service
public class ContadorServiceImpl implements ContadorService {
	
	@Autowired
	private AutomovelRepository automovelRepository;
	
	@Override
	public List<ContadorResponse> listaContador(String tipo) {

		if("D".equalsIgnoreCase(tipo)) {
			
			return listaPorDecadaDeFabricacao();
		}
		
		return listaPorFabricante();
	}
	
	private List<ContadorResponse> listaPorDecadaDeFabricacao() {
		
		Iterable<Automovel> automovelIt = automovelRepository.findAll();
		List<Automovel> autoList = new ArrayList<>();
		automovelIt.iterator().forEachRemaining(autoList::add);
		
		Map<Integer, ContadorResponse> retornoMap = new HashMap<>();
		
		for(int anoInicialDecada = 1900;anoInicialDecada <=2030;anoInicialDecada+=10) {
			
			int anoFinalDecada = anoInicialDecada+9;
			
			for (Automovel a : autoList) {
				
				int ano = a.getAno();
				if(ano >= anoInicialDecada && ano <= anoFinalDecada) {
					
					ContadorResponse decadaResponse = retornoMap.get(anoInicialDecada);
					
					if(decadaResponse != null) {
						
						decadaResponse = retornoMap.get(anoInicialDecada);
						decadaResponse.setQuantidade(decadaResponse.getQuantidade()+1);
						
					}else {
						
						decadaResponse = new ContadorResponse();
						decadaResponse.setDescricaoItem(anoInicialDecada+"");
						decadaResponse.setQuantidade(1);
						
					}
					
					retornoMap.put(anoInicialDecada, decadaResponse);
					
				}
			}
		}
		
		if(retornoMap.isEmpty()) {
			
			throw new EmptyResultDataAccessException(1);
			
		}
		
		return retornoMap.values().stream().collect(Collectors.toList());
	}

	private List<ContadorResponse> listaPorFabricante() {
		
		Iterable<Automovel> automovelIt = automovelRepository.findAll();
		Map<String, ContadorResponse> resultadoMap = new HashMap<>();
		
		for (Automovel automovel : automovelIt) {
			
			Marca marca = automovel.getMarca();
			
			ContadorResponse fabricanteResponse = resultadoMap.get(marca.getCodigo());
			
			if(fabricanteResponse != null) {
				
				fabricanteResponse.setQuantidade(fabricanteResponse.getQuantidade()+1);
				
			}else {
				fabricanteResponse = new ContadorResponse();
				fabricanteResponse.setDescricaoItem(marca.getNome());
				fabricanteResponse.setQuantidade(1);
				
			}
			
			resultadoMap.put(marca.getCodigo(), fabricanteResponse);
			
		}
		
		if(resultadoMap.isEmpty()) {
			
			throw new EmptyResultDataAccessException(1);
			
		}
		
		return resultadoMap.values().stream().collect(Collectors.toList());
	}

}
