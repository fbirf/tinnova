package br.com.tinnova.prova.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.tinnova.prova.entity.Marca;
import br.com.tinnova.prova.repository.MarcaRepository;
import br.com.tinnova.prova.service.MarcaService;

@Service
public class MarcaServiceImpl implements MarcaService{

	@Autowired
	private MarcaRepository repository;
	
	@Override
	public List<Marca> lista() {
		return repository.findAll();
	}

}
