package br.com.tinnova.prova.service;

import java.util.List;

import br.com.tinnova.prova.dto.request.AutomovelRequest;
import br.com.tinnova.prova.entity.Automovel;

public interface AutomovelService {
	
	Automovel salvar(long id,AutomovelRequest request);
	
	void excluir(long id); 
	
	List<Automovel> lista(String tipoConsulta);
	
	Automovel consulta(long id);
}
