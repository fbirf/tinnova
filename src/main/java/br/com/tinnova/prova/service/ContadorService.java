package br.com.tinnova.prova.service;

import java.util.List;

import br.com.tinnova.prova.dto.response.ContadorResponse;

public interface ContadorService {

	List<ContadorResponse> listaContador(String tipo);

}
