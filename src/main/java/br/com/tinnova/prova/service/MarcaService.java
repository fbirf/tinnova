package br.com.tinnova.prova.service;

import java.util.List;

import br.com.tinnova.prova.entity.Marca;

public interface MarcaService {
	
	List<Marca> lista();

}
