package br.com.tinnova.prova.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.tinnova.prova.dto.request.AutomovelRequest;
import br.com.tinnova.prova.entity.Automovel;
import br.com.tinnova.prova.entity.Marca;
import br.com.tinnova.prova.repository.AutomovelRepository;
import br.com.tinnova.prova.repository.MarcaRepository;
import br.com.tinnova.prova.service.AutomovelService;

@Service
public class AutomovelServiceImpl implements AutomovelService {

	@Autowired
	private AutomovelRepository automovelRepository;

	@Autowired
	private MarcaRepository marcaRepository;

	@Override
	public Automovel salvar(long id, AutomovelRequest request) {

		Optional<Marca> optMarca = marcaRepository.findById(request.getMarca());

		if (!optMarca.isPresent()) {
			throw new RuntimeException("Marca inválida");
		}

		Optional<Automovel> optAutomovel = automovelRepository.findById(id);

		Automovel automovel = null;

		if (optAutomovel.isPresent()) {

			automovel = optAutomovel.get();

		} else {

			automovel = new Automovel();
		}

		automovel.setAno(request.getAno());
		automovel.setDescricao(request.getDescricao());
		automovel.setMarca(optMarca.get());
		automovel.setUpdated(LocalDateTime.now());
		automovel.setVeiculo(request.getVeiculo());
		automovel.setVendido(request.isVendido());

		automovelRepository.save(automovel);
		return automovel;
	}

	@Override
	public void excluir(long id) {

		automovelRepository.deleteById(id);

	}

	@Override
	public Automovel consulta(long id) {

		Optional<Automovel> optAutomovel = automovelRepository.findById(id);
		if (optAutomovel.isPresent()) {

			return optAutomovel.get();

		}
		
		return null;
	}

	@Override
	public List<Automovel> lista(String tipoConsulta) {

		if (tipoConsulta == null || tipoConsulta.isEmpty()) {

			return automovelRepository.findAll();

		} else if ("NV".equalsIgnoreCase(tipoConsulta)) {

			return listaNaoVendidos();

		} else if ("US".equalsIgnoreCase(tipoConsulta)) {

			return listaRegistradosUltimaSemana();

		} else {

			throw new EmptyResultDataAccessException(1);

		}

	}

	private List<Automovel> listaNaoVendidos() {
		return automovelRepository.findByVendidoIs(false);
	}

	private List<Automovel> listaRegistradosUltimaSemana() {

		LocalDateTime hoje = LocalDateTime.now();
		LocalDateTime semanaPassada = hoje.minusDays(7);

		return automovelRepository.findByCreatedInterval(semanaPassada, hoje);
	}

}
