package br.com.tinnova.prova.exercicios;

import java.util.Arrays;

public class Exercicio2 {
	
	public static void main(String[] args) {
		
		int[] numeros = {10,3,5,6,50,30,100,300,4,99,200};
		
		ordenaBubleSort(numeros);
		
	}

	public static void ordenaBubleSort(int[] numeros) {

		int tamanho = numeros.length;

		for (int i = 0; i < tamanho; i++) {

			for(int j=i; j < tamanho;j++) {
				
				if(numeros[i] > numeros[j]) {
					int aux = numeros[j];
					numeros[j] = numeros[i];
					numeros[i] = aux;
				}
				
			}
		}
		
		System.out.println(Arrays.toString(numeros));
	}

}
