package br.com.tinnova.prova.exercicios;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class Exercicio1 {
	
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		System.out.println("Insira o Total de Eleitores:");
		long totalEleitores = s.nextLong();
		System.out.println("Insira o Total de Branccos:");
		long totalBrancos = s.nextLong();
		System.out.println("Insira o Total de Nulos:");
		long totalNulos = s.nextLong();
		
		BigDecimal votosValidos = calculaPercentualVotosValidos(totalEleitores, totalNulos, totalBrancos);
		BigDecimal votosBrancos = calculaPercentualVotosBrancos(totalEleitores, totalBrancos);
		BigDecimal votosNulos = calculaPercentualVotosNulos(totalEleitores, totalNulos);
	
		System.out.println("Percentual votos válidos: "+votosValidos+"%");
		System.out.println("Percentual votos brancos: "+votosBrancos+"%");
		System.out.println("Percentual votos nulos: "+votosNulos+"%");
		s.close();
	}
	
	public static BigDecimal calculaPercentualVotosValidos(long totalEleitores, long totalNulos, long totalBrancos) {
		
		long totalInvalidos = totalNulos + totalBrancos;
		
		BigDecimal percentual = calculaPercentual(totalEleitores,totalInvalidos);
		
		return BigDecimal.valueOf(100).subtract(percentual);
	}
	
	public static BigDecimal calculaPercentualVotosBrancos(long totalEleitores, long totalBrancos) {
		
		return calculaPercentual(totalEleitores,totalBrancos);
	}
	
	public static BigDecimal calculaPercentualVotosNulos(long totalEleitores, long totalNulos) {
		
		return calculaPercentual(totalEleitores,totalNulos);
	}

	private static BigDecimal calculaPercentual(long totalEleitores, long totalInvalidos) {

		BigDecimal percentual = BigDecimal.valueOf(totalInvalidos).multiply(BigDecimal.valueOf(100)).divide(BigDecimal.valueOf(totalEleitores),2,RoundingMode.HALF_UP);
		
		return percentual;
		
	}

}
