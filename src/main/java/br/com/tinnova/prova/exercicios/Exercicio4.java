package br.com.tinnova.prova.exercicios;

public class Exercicio4 {

	public static void main(String[] args) {

		System.out.println(somaSultiplo3e5(10));
	}

	public static int somaSultiplo3e5(int numero) {

		int resultado = 0;

		for (int i = numero-1; i >= 1; i--) {

			if ((i % 3) == 0 || (i % 5) == 0) {

				resultado += i;
			}
		}

		return resultado;

	}
}
