package br.com.tinnova.prova.exercicios;

public class Exercicio3 {
	
	public static void main(String[] args) {
		
		System.out.println(calcularFatorial(6));
		
	}
	
	public static int calcularFatorial(int numero) {
		int resultado = 1;
		
		for(int n = numero; n >=1; n--) {
			resultado*=n;
		}
		
		return resultado;
		
	}

}
