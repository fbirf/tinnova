package br.com.tinnova.prova.entity;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Automovel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String veiculo;

	@JsonIgnore
	@OneToOne
	@JoinColumn(columnDefinition = "marca",referencedColumnName = "codigo",nullable = false)
	private Marca marca;
	
	@Column(nullable = false)
	private int ano;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false)
	private boolean vendido;
	
	@JsonIgnore
	@Column(nullable = false)
	private LocalDateTime created;
	
	@JsonIgnore
	@Column(nullable = false)
	private LocalDateTime updated;
	
	@JsonProperty("marca")
	public String getCodigoMarca() {
		return marca != null ? marca.getCodigo() : null;
	}
	
	public Automovel() {
		
		this.created = LocalDateTime.now();
		
	}
	
	public LocalDateTime getCreated() {
		return created;
	}

	public String getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isVendido() {
		return vendido;
	}

	public void setVendido(boolean vendido) {
		this.vendido = vendido;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Automovel other = (Automovel) obj;
		return Objects.equals(id, other.id);
	}
	
	

	
	
}
