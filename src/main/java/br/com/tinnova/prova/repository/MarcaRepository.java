package br.com.tinnova.prova.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tinnova.prova.entity.Marca;

public interface MarcaRepository extends JpaRepository<Marca, String>{

}
