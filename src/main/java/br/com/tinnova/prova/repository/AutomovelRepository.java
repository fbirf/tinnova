package br.com.tinnova.prova.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.tinnova.prova.entity.Automovel;

@Repository
public interface AutomovelRepository extends JpaRepository<Automovel, Long>{
	
	List<Automovel> findByVendidoIs(boolean vendido);
	
	@Query("SELECT a FROM Automovel a WHERE a.created BETWEEN :dataInicial AND :dataFinal")
	List<Automovel> findByCreatedInterval(@Param("dataInicial") LocalDateTime dataInicial,@Param("dataFinal") LocalDateTime dataFinal);

}
