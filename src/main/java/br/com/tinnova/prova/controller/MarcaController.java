package br.com.tinnova.prova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tinnova.prova.entity.Marca;
import br.com.tinnova.prova.service.MarcaService;

@RestController
public class MarcaController {

	@Autowired
	private MarcaService service;

	@GetMapping("/marcas")
	public ResponseEntity<List<Marca>> listaMarcas() {

		List<Marca> list = service.lista();

		if (list.isEmpty()) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(list);
	}

}
