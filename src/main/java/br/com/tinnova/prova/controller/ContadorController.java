package br.com.tinnova.prova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tinnova.prova.dto.response.ContadorResponse;
import br.com.tinnova.prova.service.ContadorService;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/contador")
public class ContadorController {
	
	@Autowired
	private ContadorService service;
	

	@GetMapping("/{tipo}")
	public ResponseEntity<List<ContadorResponse>> listaContador( @ApiParam(example = "'D' para Década de Fabricação | 'F' para Fabricante") @PathVariable("tipo") String tipo) {

		List<ContadorResponse> list = null;
		try {
			
			list = service.listaContador(tipo);

		}catch (EmptyResultDataAccessException erda) {

			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(list);
	}

}
