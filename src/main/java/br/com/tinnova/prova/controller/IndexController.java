package br.com.tinnova.prova.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class IndexController {
	@RequestMapping("/")
	public ModelAndView index() {
	    return new ModelAndView("index");
	}
}
