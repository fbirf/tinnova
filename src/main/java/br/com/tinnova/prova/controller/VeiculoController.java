package br.com.tinnova.prova.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tinnova.prova.dto.request.AutomovelRequest;
import br.com.tinnova.prova.entity.Automovel;
import br.com.tinnova.prova.service.AutomovelService;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/veiculos")
public class VeiculoController {

	@Autowired
	private AutomovelService service;

	@PostMapping("")
	public ResponseEntity<Automovel> salvar(@RequestBody AutomovelRequest request) throws URISyntaxException {

		Automovel automovel = service.salvar(0, request);

		return ResponseEntity.created(new URI("/automovel/" + automovel.getId())).build();

	}

	@PutMapping("/{id}")
	public ResponseEntity<Automovel> atualizar(@PathVariable("id") long id, @RequestBody AutomovelRequest request)
			throws URISyntaxException {

		service.salvar(id, request);

		return ResponseEntity.noContent().build();

	}
	
	@PatchMapping("/{id}")
	public ResponseEntity<Automovel> atualizarParcialmente(@PathVariable("id") long id, @RequestBody AutomovelRequest request)
			throws URISyntaxException {

		service.salvar(id, request);

		return ResponseEntity.noContent().build();

	}

	@GetMapping("/{id}")
	public ResponseEntity<Automovel> consulta(@PathVariable("id") long id) throws URISyntaxException {

		Automovel automovel = service.consulta(id);

		if (automovel == null ) {

			return ResponseEntity.noContent().build();

		}

		return ResponseEntity.ok(automovel);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> excluir(@PathVariable("id") long id) {

		service.excluir(id);

		return ResponseEntity.noContent().build();

	}

	@GetMapping("")
	public ResponseEntity<List<Automovel>> lista(@ApiParam(example = "'NV' para Não Vendidos | 'US' para Última Semana") String tipoConsulta) {

		List<Automovel> naoVendidos = null;
		try {

			naoVendidos = service.lista(tipoConsulta);

		} catch (EmptyResultDataAccessException e) {

			return ResponseEntity.noContent().build();
			
		}

		return ResponseEntity.ok(naoVendidos);
	}

}
