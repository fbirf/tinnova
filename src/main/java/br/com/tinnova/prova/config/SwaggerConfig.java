package br.com.tinnova.prova.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.com.tinnova.prova.controller")).paths(PathSelectors.any())
				.build().useDefaultResponseMessages(false).apiInfo(apiInfo())
				.globalResponses(HttpMethod.POST, responseForPOST()).globalResponses(HttpMethod.GET, responseForGET());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Teste do Fabiano Rodrigues Felix")
				.description("API REST de teste para vaga de Java na Tinnova").version("1.0.0")
				.license("Apache License Version 2.0").licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
				.contact(new Contact("Fabiano Rodrigues Felix", "https://linkedin.com/in/fabianofelix",
						"fabiano.rodrigues.felix@hotmail.com"))
				.build();
	}

	private List<Response> responseForPOST() {

		List<Response> list = new ArrayList<>();
		list.add(new ResponseBuilder().code("201").description("Registro criado com sucesso").isDefault(true).build());
		list.add(new ResponseBuilder().code("400").description("Falha na requisição").isDefault(true).build());
		list.add(new ResponseBuilder().code("401").description("Acesso não autenticado").isDefault(true).build());
		list.add(new ResponseBuilder().code("404").description("Não encontrado").isDefault(true).build());
		list.add(new ResponseBuilder().code("500").description("Houve uma falha").isDefault(true).build());

		return list;
	}

	private List<Response> responseForGET() {

		List<Response> list = new ArrayList<>();
		list.add(new ResponseBuilder().code("200").description("Registro encontrado").isDefault(true).build());
		list.add(new ResponseBuilder().code("204").description("Registro não encontrado").isDefault(true).build());
		list.add(new ResponseBuilder().code("400").description("Falha na requisição").isDefault(true).build());
		list.add(new ResponseBuilder().code("401").description("Acesso não autenticado").isDefault(true).build());
		list.add(new ResponseBuilder().code("404").description("Não encontrado").isDefault(true).build());
		list.add(new ResponseBuilder().code("500").description("Houve uma falha").isDefault(true).build());

		return list;
	}

}
